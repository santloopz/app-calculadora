import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { PersonaComponent } from './components/persona/persona.component';
import { LoggingService } from './shared/services/LoggingService.service';
import { PersonaService } from './shared/services/persona.service';
import { AppComponent } from './app.component';
import { FormularioComponent } from './components/formulario/formulario.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonaComponent,
    FormularioComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [LoggingService, PersonaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
