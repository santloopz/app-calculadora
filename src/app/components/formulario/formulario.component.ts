import { Component, ViewChild, ElementRef } from '@angular/core';
import { LoggingService } from 'src/app/shared/services/LoggingService.service';
import { Persona } from 'src/app/shared/model/persona.model';
import { PersonaService } from 'src/app/shared/services/persona.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent {
  //two way binding
  //nombreInput:string = '';
  //apellidoInput:string = '';

  @ViewChild('nombreInput') nombreInput: ElementRef;
  @ViewChild('apellidoInput') apellidoInput: ElementRef;

  constructor(
    private logginService: LoggingService,
    private personasService: PersonaService
  ) {
    this.personasService.saludar.subscribe((indice: number) =>
      alert('El indice es: ' + indice)
    );
  }

  ngOnInit() {}

  onAgregarPersona() {
    let persona1 = new Persona(
      this.nombreInput.nativeElement.value,
      this.apellidoInput.nativeElement.value
    );
    this.logginService.enviaMesajeAConsola(
      'Enviamos persona:' + persona1.nombre + 'apellido:' + persona1.apellido
    );
    //this.personaCreada.emit(persona1);
    this.personasService.agregarPersona(persona1);
  }
}
