import { Component, Input } from '@angular/core';
import { Persona } from 'src/app/shared/model/persona.model';
import { PersonaService } from 'src/app/shared/services/persona.service';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent {

  @Input() persona: Persona;
  @Input() indice: number;

  constructor(private personasService: PersonaService) {}

  ngOnInit() {}

  emitirSaludo() {
    this.personasService.saludar.emit(this.indice);
  }

}
