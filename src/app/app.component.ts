import { Component, OnInit } from '@angular/core';
import { LoggingService } from './shared/services/LoggingService.service'
import { Persona } from './shared/model/persona.model'
import { PersonaService } from './shared/services/persona.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  titulo = 'Listado de personas';
  personas: Persona[] = [];

  constructor(private logginService: LoggingService,
              private personasService: PersonaService) {}

    ngOnInit(): void {
      this.personas = this.personasService.personas;
    }
}
